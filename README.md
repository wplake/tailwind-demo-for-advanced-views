## What is it?

This is a demo repository that demonstrates how to use Tailwind with Views and Cards in
the [Advanced Views Framework](https://wordpress.org/plugins/acf-views/).

You can copy the `tools` directory to your theme and use it in your project. If you're building from scratch,
check [our starter theme with Tailwind](https://gitlab.com/wplake/avf-tailwind-starter-theme).

### 1. How it works

The AVF offers the [File system storage](https://docs.acfviews.com/templates/file-system-storage)
option. When enabled, View and Card templates and assets are stored in the `advanced-views` folder within the current
theme.

This repository illustrates how to use Tailwind in the Twig templates of Views and Cards. These are independent
components, and our intention is to get Tailwind classes from the `custom.twig` file of a component and put them into
the `style.css` of the same component.

### 2. Requirements

#### 2.1) `style.scss`

Every View and Card folder has `style.css` and `custom.twig` files, which are created automatically during component
creation.

For Tailwind usage, we need to create a `style.scss` file for every View or Card. This can be achieved in a couple of
ways:

1. Filling the Sass template field in the Defaults tab of the AVF settings.
2. Manually creating the file for Views and Cards.

We recommend the first approach, as it will create the `style.scss` file automatically for every new View or Card.

Note: The `style.scss` and `script.ts` filenames
are [reserved and supported by AVF](https://docs.acfviews.com/templates/file-system-storage#sass-and-typescript-usage).
This means that during import/export, they will be processed automatically like other files.

The `style.scss` file must include the following content:

```scss
/* advanced-views:tailwind */
@tailwind components;
@tailwind utilities;
```

**Explanation:**

1. `@tailwind components;@tailwind utilities;` - Tailwind's directives, which will be replaced with the CSS code for the
   used classes. We
   skip the `@base` layer, as they are global and shouldn't be included in every component.
2. `/* advanced-views:tailwind */` - a comment that tells Advanced Views that the file contains Tailwind-related styles.
   In this case, during the assets enqueueing on the page, the plugin will merge Tailwind-related styles from all the
   components to avoid duplications (e.g., if you used the same class inside several different components that are
   present on the current page).

While the content above is necessary, you still can add your own CSS styles above or below it.

#### 2.2) `tailwind.scss` (optional)

Besides the classes themselves, Tailwind comes with a set of global styles that can be referred to as reset or base.

While Tailwind classes will work even without these styles, it's recommended to include them in your project.

The current setup includes `tailwind.scss`, which compiles into `tailwind.css`. This name is reserved and will be
enqueued by the plugin automatically on pages where at least one Tailwind-related component appears.

The file must include the following content:

```scss
@tailwind base;
```

### 3. How to use it

1. Copy the `tools` directory to your theme and enter this folder.
2. Run `corepack use yarn@latest` (since v2, yarn is integrated into nodejs,
   and [can be activated](https://yarnpkg.com/getting-started/install) on any folder).
4. Run `yarn install`.
5. Use `yarn watch/build` commands to turn Tailwind classes from `custom.twig` files into CSS inside their
   sibling `style.css` files.

### 4. Tailwind configuration

Tailwind CLI supports the `tailwind.config.js` file, so you can employ it as usual. The only difference is that
you don't need to manually declare `.twig` files/regex in the `content` section, as it's scanned dynamically during the
execution.

Note: The current Tailwind config uses the `content` section only as a part of the Tailwind transform feature to add
class
extraction from `data-wp-class--x` attributes, which are part of
the [WordPress Interactivity API](https://docs.acfviews.com/templates/wordpress-interactivity-api).

If you are not going to use the WordPress Interactivity API, you can safely remove the entire content section.

```
content: {
   files: ['../advanced-views/**/*.twig'],
   transform: {
      twig: (content) => {
         return content.replace(/data-wp-class--([^=]+)=["'][^'"]+['"]/g, 'class="$1"');
      }
   }
}
```

### 4. Implementation details

This repository utilizes the official [Tailwind CLI](https://tailwindcss.com/docs/installation) for compiling and
the [Chokidar CLI](https://www.npmjs.com/package/chokidar-cli) for watching changes.

Check the `scripts` section of the `tools/package.json` for details.

Helper scripts:

1. `build-tailwind-all.js` - uses `glob` to get a list of all the `.twig` templates and runs `build-tailwind-single.js`
   for each.
2. `build-tailwind-single.js` runs the Tailwind CLI for a single `.twig` file, passes the sibling `style.scss` as input,
   and `style.css` as output.

The current setup only compiles Tailwind classes without minifying them. We do not recommend minifying assets for the
following reasons:

**a) Loss of compatibility with the Advanced Views on-site code editor**

Once assets are minified, it becomes impossible to edit them using the on-site code editor. This means that if you hand
off the project, the next developer won't be able to make even small changes without delving into the theme.

Additionally, you'll lose the ability to make quick amendments, which are sometimes necessary for urgent fixes on a live
website.

**b) Built-in minification in the Advanced Views plugin**

The Advanced Views plugin includes basic asset minification functionality. While it may not be as powerful as Webpack or
Laravel Mix, it's sufficient for most use cases.