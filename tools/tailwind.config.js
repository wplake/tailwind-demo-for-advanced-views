/** @type {import('tailwindcss').Config} */
module.exports = {
    theme: {
        colors: {
            'blue': '#144b6a',
        },
    },
    content: {
        files: ['../advanced-views/**/*.twig'],
        transform: {
            twig: (content) => {
                return content.replace(/data-wp-class--([^=]+)=["'][^'"]+['"]/g, 'class="$1"');
            }
        }
    },
}