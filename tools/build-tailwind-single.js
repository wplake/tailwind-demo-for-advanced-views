const {exec} = require('child_process');
const fs = require('fs');

let event = process.argv[2] || '';
let targetFile = process.argv[3] || '';

if ('' === event ||
    '' === targetFile) {
    console.log('Call is missing necessary args')
    process.exit(1);
}

// ignore if the file is deleted.
if ('unlink' === event) {
    process.exit(0);
}

let isTwigSource = targetFile.endsWith('.twig');

let targetSassFile = true === isTwigSource ?
    targetFile.replace('custom.twig', 'style.scss') :
    targetFile;
let targetCssFile = true === isTwigSource ?
    targetFile.replace('custom.twig', 'style.css') :
    targetFile.replace('style.scss', 'style.css');
let targetTwigFile = true === isTwigSource ?
    targetFile :
    targetFile.replace('style.scss', 'custom.twig');

// skip if .scss file does not exist (e.g. Tailwind is not used)
if (false === fs.existsSync(targetSassFile)) {
    process.exit(0);
}

exec(`npx tailwindcss -i ${targetSassFile} -o ${targetCssFile} --content ${targetTwigFile}`, (err, stdout, stderr) => {
    console.log('Tailwind: ' + stdout + stderr);
});
