const glob = require('glob');
const {exec} = require('child_process');

glob.sync('../advanced-views/**/custom.twig').forEach((file) => {
    exec(`node ./build-tailwind-single change ${file}`, (err, stdout, stderr) => {
        console.log(stdout + stderr);
    });
});
